﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithGitExample
{
    class Program
    {
        static void Main(string[] args)
        {
            AskhatHomeWork();
            Umut();
            AzamatHw();
        }

        static void AskhatHomeWork()
        {

        }

        static void AzamatHw()
        {
            Console.WriteLine("Enter text");
            string text = Console.ReadLine().ToLower();
            text = text.Replace(",", "");

            string[] arrayWords = text.Split(' ');

            var words = arrayWords.GroupBy(x => x)
                                  .Select(x => new { word = x.Key, count = x.Count() });

            foreach (var i in words)
            {
                Console.WriteLine($"{i.count}-{i.word}");
            }
        }

        static void Umut()
        {
            Console.WriteLine("Это мой метод");

        }
    }
}
